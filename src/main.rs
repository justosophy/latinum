use anyhow::{Context, Result};
use clap::Parser;
use std::{
    io,
    io::{BufRead, Write},
    os::unix::prelude::MetadataExt,
    path::Path,
};

#[derive(Parser, Debug)]
struct Cli {
    #[clap(parse(from_os_str))]
    path: std::path::PathBuf,
}

fn main() -> Result<()> {
    let args = Cli::parse();

    let path_metadata = Path::new(&args.path).symlink_metadata()?;
    if !path_metadata.is_file() {
        return Err(io::Error::new(io::ErrorKind::NotFound, "Not a file"))
            .with_context(|| "Not a file");
    } else if path_metadata.size() == 0 {
        return Err(io::Error::new(io::ErrorKind::Other, "File was empty"))
            .with_context(|| "File was empty");
    }

    let file = std::fs::File::open(&args.path)
        .with_context(|| format!("Unable to open {:?}", &args.path))?;
    let reader = std::io::BufReader::new(file);

    let stdout = io::stdout();
    let mut handle = stdout.lock();

    let line = reader
        .lines()
        .into_iter()
        .next()
        .unwrap()
        .with_context(|| "Unable to iterate lines")?;

    writeln!(handle, "First line of file:").unwrap();
    writeln!(handle, "{}", line).unwrap();

    Ok(())
}
